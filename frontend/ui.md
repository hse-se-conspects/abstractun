1. Side menu с выбором годом выпуска.

2. Для каждого года выпуска отображается страничка, вверху которой отображается
меню для выбора текущего курса ("кнопки" с выпадающим меню для выбора семестра).

3. Далее на странице отображаются только конспекты выбранного года и семестра. 

4. Отображение только названия и даты последнего обновления, сортировка по названию.

5. Кликабельное название конспекта (выдает pdf).