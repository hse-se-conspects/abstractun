### Внешние запросы к API

* ``/api/v1/conspect-list``

  Возвращает JSON

```JSON
{
  "conspects": [
    {
      "source": "spbau-inf-2020",
      "conspect": "term179-flask",
      "title": "Flask для чайников",
      "lastUpdate": /* unix-time */,
      "period": {
        "term": 5,
        "module": 1
      },
      "groupId": {
        "owner": "se",
        "graduation": 2020
      }
    },
    {
      "source": "burunduk",
      "conspect": "out",
      "title": "Старый конспект Серёжи",
      "lastUpdate": /* unix-time */,
      "period": {
        "term": 3
      },
      "groupId": {
        "owner": "se",
        "graduation": 2020
      },
      "author": "burunduk1"
    },
    /*...*/
  ]
}
```

Схему (в формате protobuf) с указанием обязательности и комментариями к полям можно найти в файле `backend/api_structure.proto`.


* ``api/v1/get?source=spbau-inf-2020&conspect=flask``

Выдаёт честную pdf-ку.

### Внутренние запросы к API

* ``api/v1/private/update?source=...``

### Сущности, которые мы обязательно

* логирование
* Сережа
